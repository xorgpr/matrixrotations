﻿using Autofac;
using MatrixRotations.Services;
using Autofac.Integration.Mvc;
using System.Web.Mvc;

namespace MatrixRotations.Util
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<MatrixService>().As<IMatrixService>();
            builder.RegisterType<CSVService>().As<ICSVService>();

            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}