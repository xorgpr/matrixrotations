﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MatrixRotations.Models
{
    public class Matrix
    {
        public int Size{ get; }
        public int[,] Data { get; set; }

        public int this[int n, int t]
        {
            get { return Data[n, t]; }
            set { Data[n, t] = value; }
        }

        public Matrix(int Size)
        {
            this.Size = Size;
            Data = new int[this.Size, this.Size];
        }

        public Matrix(int Size, params int[] array) : this(Size)
        {
            if (array.Length < Size * Size)
                throw new Exception("incorrect size array");

            var enumerator = array.GetEnumerator();

            for (int n = 0; n < Size; ++n)
            {
                for (int t = 0; t < Size; ++t)
                {
                    enumerator.MoveNext();
                    Data[n, t] = (int)enumerator.Current;
                }
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder("");

            for (int n = 0; n < Size; ++n)
            {
                for (int t = 0; t < Size; ++t)
                {
                    result.Append(this[n, t]);
                    if (t == Size - 1)
                    {
                        break;
                    }

                    result.Append(", ");
                }
                if (n == Size - 1)
                {
                    break;
                }

                result.Append(Environment.NewLine);
            }

            return result.ToString();
        }
    }
}