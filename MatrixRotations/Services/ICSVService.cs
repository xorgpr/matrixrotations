﻿using MatrixRotations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixRotations.Services
{
    public interface ICSVService
    {
        List<int> GenerateMatrixFromCSV(string matrixString);
        string GenerateCSVFromMatrix(Matrix matrix);
    }
}
