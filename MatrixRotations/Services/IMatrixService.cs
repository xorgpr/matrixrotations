﻿using System.Collections.Generic;
using MatrixRotations.Models;

namespace MatrixRotations.Services
{
    public interface IMatrixService
    {
        Matrix CopyArrayToMatrix(List<int> matrixList);
        Matrix GenerateRandomMatrix();
        Matrix GenerateRandomMatrix(int Size);
        void RotateMatrixLeft(ref Matrix matrix);
        void RotateMatrixRight(ref Matrix matrix);
    }
}