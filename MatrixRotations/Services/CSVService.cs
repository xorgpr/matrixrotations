﻿using MatrixRotations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MatrixRotations.Services
{
    public class CSVService: ICSVService
    {
        private const string comaSeparator = ",";
        private const string lineSeparator = "\r\n";

        public List<int> GenerateMatrixFromCSV(string matrixString)
        {
            var result = matrixString.Split(new[] { lineSeparator }, StringSplitOptions.RemoveEmptyEntries)
                .SelectMany(i => i.Split(new[] { comaSeparator }, StringSplitOptions.RemoveEmptyEntries))
                .Select(s => Int32.Parse(s)).ToList();

            return result;
        }

        public string GenerateCSVFromMatrix(Matrix matrix)
        {
            StringBuilder result = new StringBuilder("");

            for (int n = 0; n < matrix.Size; ++n)
            {
                for (int t = 0; t < matrix.Size; ++t)
                {
                    result.Append(matrix[n, t]);
                    if (t == matrix.Size - 1)
                    {
                        break;
                    }

                    result.Append(comaSeparator);
                }
                if (n == matrix.Size - 1)
                {
                    break;
                }

                result.Append(lineSeparator);
            }

            return result.ToString();
        }
    }
}