﻿using MatrixRotations.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MatrixRotations.Services
{
    public class MatrixService : IMatrixService
    {
        public Matrix GenerateRandomMatrix()
        {
            Random random = new Random();

            Matrix matrix = GenerateRandomMatrix(random.Next(1, 10));

            GenerateRandomMatrixContext(ref matrix);

            return matrix;
        }

        public Matrix GenerateRandomMatrix(int Size)
        {
            Matrix matrix = new Matrix(Size);

            GenerateRandomMatrixContext(ref matrix);

            return matrix;
        }

        public Matrix CopyArrayToMatrix(List<int> matrixList)
        {
            Matrix matrix = new Matrix((int)(Math.Sqrt(matrixList.Count) + 0.00001));

            var enumerator = matrixList.GetEnumerator();

            for (int n = 0; n < matrix.Size; ++n)
            {
                for (int t = 0; t < matrix.Size; ++t)
                {
                    enumerator.MoveNext();
                    matrix[n, t] = enumerator.Current;
                }
            }

            return matrix;
        }

        public void RotateMatrixLeft(ref Matrix matrix)
        {
            for (int i = 0; i < matrix.Size / 2; i++)
            {
                for (int j = i; j < matrix.Size - 1 - i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[j, matrix.Size - 1 - i];
                    matrix[j, matrix.Size - 1 - i] = matrix[matrix.Size - 1 - i, matrix.Size - 1 - j];
                    matrix[matrix.Size - 1 - i, matrix.Size - 1 - j] = matrix[matrix.Size - 1 - j, i];
                    matrix[matrix.Size - 1 - j, i] = tmp;
                }
            }
        }

        public void RotateMatrixRight(ref Matrix matrix)
        {
            for (int i = 0; i < matrix.Size / 2; i++)
            {
                for (int j = i; j < matrix.Size - 1 - i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[matrix.Size - j - 1, i];
                    matrix[matrix.Size - j - 1, i] = matrix[matrix.Size - i - 1, matrix.Size - j - 1];
                    matrix[matrix.Size - i - 1, matrix.Size - j - 1] = matrix[j, matrix.Size - i - 1];
                    matrix[j, matrix.Size - i - 1] = tmp;
                }
            }
        }

        private void GenerateRandomMatrixContext(ref Matrix matrix)
        {
            Random random = new Random();

            for (int n = 0; n < matrix.Size; ++n)
            {
                for (int t = 0; t < matrix.Size; ++t)
                {
                    matrix[n, t] = random.Next(1, 100);
                }
            }
        }
    }
}