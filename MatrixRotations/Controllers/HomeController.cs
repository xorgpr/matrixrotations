﻿using MatrixRotations.Models;
using MatrixRotations.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatrixRotations.Controllers
{
    public class HomeController : Controller
    {
        private IMatrixService matrixService;
        private ICSVService csvService;

        public HomeController(IMatrixService matrixService, ICSVService csvService)
        {
            this.matrixService = matrixService;
            this.csvService = csvService;
        }

        public ActionResult Index()
        {
            var matrix = matrixService.GenerateRandomMatrix();

            return View(matrix);
        }

        public ActionResult GenNew()
        {
            var matrix = matrixService.GenerateRandomMatrix();
            return View("Index", matrix);
        }

        [HttpPost]
        public ActionResult RotateLeft(List<int> matrix)
        {
            var matrixFromList = matrixService.CopyArrayToMatrix(matrix);
            matrixService.RotateMatrixLeft(ref matrixFromList);
            var resultMatrix = matrixFromList;
            return View("Index", resultMatrix);
        }

        [HttpPost]
        public FileContentResult SaveCsv(List<int> matrix)
        {
            var matrixFromList = matrixService.CopyArrayToMatrix(matrix);
            var csv = csvService.GenerateCSVFromMatrix(matrixFromList);
            
            return File(new System.Text.UTF8Encoding().GetBytes(csv), 
                "text/csv", "matrix" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + matrixFromList.Size + ".csv");
        }

        [HttpPost]
        public ActionResult LoadCsv(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                BinaryReader binaryReader = new BinaryReader(upload.InputStream);
                byte[] binData = binaryReader.ReadBytes(upload.ContentLength);

                string result = System.Text.Encoding.UTF8.GetString(binData);

                var matrixList = csvService.GenerateMatrixFromCSV(result);

                var matrix = matrixService.CopyArrayToMatrix(matrixList);

                return View("Index", matrix);
            }
            return RedirectToAction("Index");
        }
    }
}