﻿using MatrixRotations.Controllers;
using MatrixRotations.Models;
using MatrixRotations.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MatrixRotations.Tests
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void index()
        {
            var matrixService = new Mock<IMatrixService>();
            var csvService = new Mock<ICSVService>();

            matrixService.Setup(r => r.GenerateRandomMatrix()).Returns(new Matrix(3));
            
            HomeController controller = new HomeController(matrixService.Object, csvService.Object);
            
            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual<int>(3, ((Matrix)result.Model).Size);
        }

        [TestMethod]
        public void index1()
        {
            var matrixService = new Mock<IMatrixService>();
            var csvService = new Mock<ICSVService>();

            matrixService.Setup(r => r.GenerateRandomMatrix()).Returns(new Matrix(2, 1, 2, 3, 4));

            HomeController controller = new HomeController(matrixService.Object, csvService.Object);

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual<int>(2, ((Matrix)result.Model).Size);
            Assert.AreEqual<string>(@"1, 2
3, 4", ((Matrix)result.Model).ToString());
        }

        [TestMethod]
        public void GenNew()
        {
            var matrixService = new Mock<IMatrixService>();
            var csvService = new Mock<ICSVService>();

            matrixService.Setup(r => r.GenerateRandomMatrix()).Returns(new Matrix(2, 1, 2, 3, 4));

            HomeController controller = new HomeController(matrixService.Object, csvService.Object);

            ViewResult result = controller.GenNew() as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual<int>(2, ((Matrix)result.Model).Size);
            Assert.AreEqual<string>(@"1, 2
3, 4", ((Matrix)result.Model).ToString());
        }

        [TestMethod]
        public void RotateLeft()
        {
            var matrixService = new Mock<IMatrixService>();
            var csvService = new Mock<ICSVService>();

            matrixService.Setup(r => r.CopyArrayToMatrix(It.IsAny<List<int>>())).Returns(new Matrix(2, 1, 2, 3, 4));

            var tornedMatrix = new Matrix(2, 5, 4, 3, 2);
            matrixService.Setup(r => r.RotateMatrixLeft(ref tornedMatrix));
            
            HomeController controller = new HomeController(matrixService.Object, csvService.Object);

            ViewResult result = controller.RotateLeft(new List<int>() { 1, 2, 3, 4 }) as ViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual<int>(2, ((Matrix)result.Model).Size);
            Assert.AreEqual<string>(@"1, 2
3, 4", ((Matrix)result.Model).ToString());
        }

        [TestMethod]
        public void SaveCsv()
        {
            var matrixService = new Mock<IMatrixService>();
            var csvService = new Mock<ICSVService>();

            matrixService.Setup(r => r.CopyArrayToMatrix(It.IsAny<List<int>>())).Returns(new Matrix(2));
            csvService.Setup(r => r.GenerateCSVFromMatrix(It.IsAny<Matrix>())).Returns(@"1, 2
3, 4");

            HomeController controller = new HomeController(matrixService.Object, csvService.Object);

            FileContentResult result = controller.SaveCsv(new List<int>() { 1, 2, 3, 4 }) as FileContentResult;
            var resultString = Encoding.UTF8.GetString(result.FileContents);
            
            Assert.AreEqual<string>(@"1, 2
3, 4", resultString);
        }


    }
}
