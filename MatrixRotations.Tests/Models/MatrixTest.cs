﻿using MatrixRotations.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixRotations.Tests
{
    [TestClass]
    public class MatrixTest
    {
        [TestMethod]
        public void MatrixInitializeSize()
        {
            Matrix matrix = new Matrix(4);

            Assert.AreEqual<int>(4, matrix.Size);
        }

        [TestMethod]
        public void MatrixInitializeSizeData()
        {
            Matrix matrix = new Matrix(3);
            matrix.Data[1, 2] = 5;
            matrix.Data[2, 0] = 7;

            Assert.AreEqual<int>(3, matrix.Size);
            Assert.AreEqual<int>(5, matrix[1, 2]);
            Assert.AreEqual<int>(7, matrix[2, 0]);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void MatrixInitializeIncorrect()
        {
            Matrix matrix = new Matrix(3, 1, 2, 3);
        }

        [TestMethod]
        public void MatrixInitializeArrayParam()
        {
            Matrix matrix = new Matrix(3, 1, 2, 3, 4, 5, 6, 7, 8, 9);

            Assert.AreEqual<int>(3, matrix.Size);
            Assert.AreEqual<string>(@"1, 2, 3
4, 5, 6
7, 8, 9", matrix.ToString());
        }

        [TestMethod]
        public void MatrixInitializeArray()
        {
            Matrix matrix = new Matrix(2, new int[] { 1, 2, 3, 4 });
            
            Assert.AreEqual<int>(2, matrix.Size);
            Assert.AreEqual<string>(@"1, 2
3, 4", matrix.ToString());
        }
    }
}
