﻿using MatrixRotations.Models;
using MatrixRotations.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixRotations.Tests
{
    [TestClass]
    public class CSVServiceTest
    {
        [TestMethod]
        public void GenerateMatrixFromCSV()
        {
            ICSVService csvService = new CSVService();
            var matrix = csvService.GenerateMatrixFromCSV(@"13,70
96,98");

            Assert.AreEqual<int>(4, matrix.Count);

            Assert.AreEqual<int>(13, matrix[0]);
            Assert.AreEqual<int>(70, matrix[1]);
            Assert.AreEqual<int>(96, matrix[2]);
            Assert.AreEqual<int>(98, matrix[3]);
        }

        [TestMethod]
        public void GenerateCSVFromMatrix()
        {
            ICSVService csvService = new CSVService();
            var csv = csvService.GenerateCSVFromMatrix(new Matrix(2, new int[] { 1, 7, 5, 9 }));

            Assert.AreEqual<string>(@"1,7
5,9", csv);
        }
    }
}