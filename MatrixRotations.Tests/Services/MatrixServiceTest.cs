﻿using MatrixRotations.Models;
using MatrixRotations.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixRotations.Tests
{
    [TestClass]
    public class MatrixServiceTest
    {
        [TestMethod]
        public void GenerateRandomMatrix()
        {
            IMatrixService matrixService = new MatrixService();
            var matrix = matrixService.GenerateRandomMatrix();

            Assert.IsNotNull(matrix);
        }

        [TestMethod]
        public void GenerateRandomMatrixCtr()
        {
            IMatrixService matrixService = new MatrixService();
            var matrix = matrixService.GenerateRandomMatrix(7);

            Assert.AreEqual<int>(7, matrix.Size);
        }

        [TestMethod]
        public void CopyArrayToMatrix()
        {
            IMatrixService matrixService = new MatrixService();
            var matrix = matrixService.CopyArrayToMatrix(new List<int>() { 3, 2, 7, 6 });

            Assert.AreEqual<int>(2, matrix.Size);
            Assert.AreEqual<string>(@"3, 2
7, 6", matrix.ToString());
        }

        [TestMethod]
        public void RotateMatrixLeft()
        {
            IMatrixService matrixService = new MatrixService();
            var matrix = new Matrix(2, new int[] { 1, 7, 5, 9 });

            matrixService.RotateMatrixLeft(ref matrix);

            Assert.AreEqual<int>(2, matrix.Size);
            Assert.AreEqual<string>(@"7, 9
1, 5", matrix.ToString());
        }

        [TestMethod]
        public void RotateMatrixRight()
        {
            IMatrixService matrixService = new MatrixService();
            var matrix = new Matrix(2, new int[] { 1, 7, 5, 9 });

            matrixService.RotateMatrixRight(ref matrix);

            Assert.AreEqual<int>(2, matrix.Size);
            Assert.AreEqual<string>(@"5, 1
9, 7", matrix.ToString());
        }
    }
}
