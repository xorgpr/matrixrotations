﻿using System;
using System.Linq;
using MatrixRotations.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MatrixRotations.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private const string comaSeparator = ",";
        private const string lineSeparator = "\r\n";

        [TestMethod]
        public void TestMethod1()
        {
            Matrix matrix = new Matrix(4);

            int matrixValue = 1;

            for (int n = 0; n < matrix.Size; ++n)
            {
                for (int t = 0; t < matrix.Size; ++t)
                {
                    matrix[n, t] = matrixValue++;
                }
            }

            for (int i = 0; i < matrix.Size / 2; i++)
            {
                for (int j = i; j < matrix.Size - 1 - i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[j, matrix.Size - 1 - i];
                    matrix[j, matrix.Size - 1 - i] = matrix[matrix.Size - 1 - i, matrix.Size - 1 - j];
                    matrix[matrix.Size - 1 - i, matrix.Size - 1 - j] = matrix[matrix.Size - 1 - j, i];
                    matrix[matrix.Size - 1 - j, i] = tmp;
                }
            }
            
            for (int i = 0; i < matrix.Size / 2; i++)
            {
                for (int j = i; j < matrix.Size - 1 - i; j++)
                {
                    int tmp = matrix[i, j];
                    matrix[i, j] = matrix[matrix.Size - j - 1, i];
                    matrix[matrix.Size - j - 1, i] = matrix[matrix.Size - i - 1, matrix.Size - j - 1];
                    matrix[matrix.Size - i - 1, matrix.Size - j - 1] = matrix[j, matrix.Size - i - 1];
                    matrix[j, matrix.Size - i - 1] = tmp;
                }
            }
        }
        
        [TestMethod]
        public void TestMethod2()
        {
            string matrixString = @"34,77,27,31
90,39,6,62
95,93,11,33
41,88,83,81";

            var result = matrixString.Split(new[] { lineSeparator }, StringSplitOptions.RemoveEmptyEntries)
                .SelectMany(i => i.Split(new[] { comaSeparator }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}
